# Changelog

<!-- 
added: New feature
fixed: Bug fix
changed: Feature change
deprecated: New deprecation
removed: Feature removal
security: Security fix
performance: Performance improvement
other: Other 
-->

## [Unreleased]
- Sorry for my English, I use a translator. :)

## [v1] - 2022-05-06
### Added
- Initial release.

## [v2] - 2022-05-10
### Added
- Added Italian language

