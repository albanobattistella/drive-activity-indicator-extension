# Drive Activity Indicator Extension

**Gnome extension to visualize the activity of storage drives (disk activity LED simulator)**

![Banner](/images/banner.svg)

## Install
1. Open [Drive Activity Indicator] on GNOME Shell Extensions site.
2. Click slider to install extension.

[Drive Activity Indicator]: https://extensions.gnome.org/extension/6955/drive-activity-indicator/


## Translations
You can help us translating extension to your language.

[Translate on Weblate](https://hosted.weblate.org/engage/drive-activity-indicator/)

[![Estado de la traducción](https://hosted.weblate.org/widgets/drive-activity-indicator/-/horizontal-auto.svg)](https://hosted.weblate.org/engage/drive-activity-indicator/)
